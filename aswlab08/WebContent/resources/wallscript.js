var apiURI = "http://localhost:8080/aswlab08/api/tweets";

var req;
var tweetBlock = "	<div id='tweet_{0}' class='wallitem'>\n\
	<div class='likes'>\n\
	<span class='numlikes'>{1}</span><br /> <span\n\
	class='plt'>people like this</span><br /> <br />\n\
	<button onclick='{5}Handler(\"{0}\")'>{5}</button>\n\
	<br />\n\
	</div>\n\
	<div class='item'>\n\
	<h4>\n\
	<em>{2}</em> on {4}\n\
	</h4>\n\
	<p>{3}</p>\n\
	</div>\n\
	</div>\n";

String.prototype.format = function() {
	var args = arguments;
	return this.replace(/{(\d+)}/g, function(match, number) { 
		return typeof args[number] != 'undefined'
			? args[number]
		: match
		;
	});
};

function likeHandler(tweetID) {
	var target = 'tweet_'+tweetID;
	var uri = apiURI + "/" + tweetID + "/like";
	req = new XMLHttpRequest();
	req.open('GET', uri, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			document.getElementById(target).getElementsByClassName("numlikes")[0].innerHTML = req.responseText;
		}
	};
	req.send(/*no params*/null);
}


function getTweetHTML(tweet, action) {  // action :== "like" xor "delete"
	var dd = tweet.tweetDate +" @ " + tweet.tweetHour;
	return tweetBlock.format(tweet.tweetID, tweet.likes, tweet.authorName, tweet.content, dd, action);
	
}

function getTweets() {
	req = new XMLHttpRequest(); 
	req.open("GET", apiURI, true); 
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			var tweet_list = "";
			var tweets = JSON.parse(req.responseText);
			for (var i=0; i<tweets.length; i++) {
				var tt = tweets[i];
				var action = "like";
				tweet_list += getTweetHTML(tt, action);
			};
			document.getElementById("tweet_list").innerHTML = tweet_list;
		};
	};
	req.send(null); 
};


//main
function main() {
	getTweets();
};
